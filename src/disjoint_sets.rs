//! Contains [`DisjointSets`](crate::disjoint_sets::DisjointSets) which is the main implementation
//! of the disjoint-set forest in order to support the union-find algorithm.
//! [`DisjointSets`](crate::disjoint_sets::DisjointSets) uses union-by-rank and path-compression
//! to improve the algorithmic complexity.
//!
//! See [`DisjointSets`](crate::disjoint_sets::DisjointSets) for more details.

use crate::{
    node::Node,
    traits::{Error, Result, UnionFind},
};
use std::{
    cmp::Ordering::*,
    collections::{HashMap, HashSet},
    default::Default,
    hash::Hash,
};

/// An implementation of a disjoint-set forest that supports the union-find algorithm.
///
/// This implementation uses _union-by-rank_ as well as _path-compression_ heuristics to
/// improve the algorithmic complexity.
///
/// See the trait [`UnionFind`](crate::traits::UnionFind), which this struct implements, to
/// see the main operations that this struct supports.
///
/// # Algorithmic Complexity
///
/// As mentioned before, this implementation uses _union-by-rank_ as well as _path-compression_
/// heuristics to improve the algorithmic complexity.
///
/// As noted in the [Wikipedia article](https://en.wikipedia.org/wiki/Disjoint-set_data_structure),
/// this implies that a sequence of `m` calls to `find_set`, `make_set` and `union` requires `O(mα(n))`
/// time where `α(n)` is the [inverse Ackermann
/// function](https://en.wikipedia.org/wiki/Inverse_Ackermann_function). For all practical
/// purposes, you can think of each operation as being essentially `O(1)`.
///
/// # Accessing the Disjoint Sets
///
/// In order to allow easy access to the disjoint sets stored in the forest, this struct implements
/// `IntoIterator`:
/// ```
/// use union_find_rs::prelude::*;
/// use std::collections::HashSet;
///
/// let mut sets: DisjointSets<usize> = DisjointSets::new();
///
/// sets.make_set(1).unwrap();
/// sets.make_set(4).unwrap();
/// sets.make_set(9).unwrap();
///
/// sets.union(&4, &9).unwrap();
///
/// // `into_iter` returns an iterator whose item is of type `HashSet<T>`
/// let as_vec: Vec<HashSet<usize>> = sets.into_iter().collect();
///
/// // there should be 2 disjoint sets, where one of them only contains `4` and the other one
/// // only contains `9`
/// assert_eq!(as_vec.len(), 2);
/// assert!(
///     as_vec
///         .iter()
///         .any(|set| set == &vec![1].into_iter().collect::<HashSet<usize>>())
/// );
/// assert!(
///     as_vec
///         .iter()
///         .any(|set| set == &vec![4, 9].into_iter().collect::<HashSet<usize>>())
/// );
/// ```
///
/// # References
///
/// This struct was implemented using ideas from the following:
///
/// 1. [Introduction to Algorithms](https://en.wikipedia.org/wiki/Introduction_to_Algorithms)
/// 1. [Disjoint-set data structure](https://en.wikipedia.org/wiki/Disjoint-set_data_structure)
pub struct DisjointSets<T>
where
    T: Copy + Eq + Hash,
{
    nodes: HashMap<T, Node<T>>,
    strategy: FindStrategy,
}

/// Heuristics to use in order to improve the algorithmic complexity of the algorithm.
///
/// These heuristics will affect how `find_set` modifies the forest.
///
/// See the [Wikipedia article](https://en.wikipedia.org/wiki/Disjoint-set_data_structure) for more
/// information about these heuristics.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum FindStrategy {
    /// during `find_set(x)`, for all nodes in the path from `x` to the root node, including `x`
    /// but excluding the root node, set their parent to the root node
    PathCompression,

    /// during `find_set(x)`, for all nodes in the path from `x` to the root node, including `x`
    /// but excluding the root node, set their parent to their grand parent
    PathSplitting,
}

//
impl Default for FindStrategy {
    fn default() -> Self {
        FindStrategy::PathCompression
    }
}

//
impl<T> UnionFind<T> for DisjointSets<T>
where
    T: Copy + Eq + Hash,
{
    /// See [`UnionFind`](crate::traits::UnionFind) for an example.
    fn make_set(&mut self, item: T) -> Result<()> {
        //
        match self.contains(&item) {
            // if `item` alreday exists, error with the approriate variant
            true => Err(Error::ItemAlreadyExists),
            // only make the set if it doesn't already exist
            false => {
                //
                let node = Node::new(item, 0);
                self.nodes.insert(item, node);
                //
                Ok(())
            }
        }
    }

    /// See [`UnionFind`](crate::traits::UnionFind) for an example.
    fn union(&mut self, x: &T, y: &T) -> Result<()> {
        //
        match x == y {
            // if they are equivalent, no need to merge their sets
            true => Ok(()),
            // merge the two sets by first finding their representatives
            false => self.link(
                &self.find_set(x)?, //
                &self.find_set(y)?, //
            ),
        }
    }

    /// See [`UnionFind`](crate::traits::UnionFind) for an example.
    fn find_set(&self, item: &T) -> Result<T> {
        //
        use crate::disjoint_sets::FindStrategy::*;
        //
        match self.strategy {
            PathCompression => self.find_set_path_compression(item),
            PathSplitting => self.find_set_path_splitting(item),
        }
    }
}

//
impl<T> DisjointSets<T>
where
    T: Copy + Eq + Hash,
{
    /// Creates an empty instance of `DisjointSets`.
    ///
    /// # Example
    ///
    /// ```
    /// use union_find_rs::prelude::*;
    ///
    /// let mut sets: DisjointSets<usize> = DisjointSets::new();
    /// ```
    pub fn new() -> Self {
        Self {
            nodes: HashMap::new(),
            strategy: Default::default(),
        }
    }

    /// Creates an empty instance of `DisjointSets` with a specific strategy to be used by the
    /// `find_set` operation.
    ///
    /// See [FindStrategy](crate::disjoint_sets::FindStrategy).
    ///
    /// # Example
    ///
    /// ```
    /// use union_find_rs::prelude::*;
    ///
    /// let mut sets_1: DisjointSets<usize> = DisjointSets::with_strategy(FindStrategy::PathCompression);
    ///
    /// let mut sets_2: DisjointSets<usize> = DisjointSets::with_strategy(FindStrategy::PathSplitting);
    /// ```
    pub fn with_strategy(strategy: FindStrategy) -> Self {
        Self {
            nodes: HashMap::new(),
            strategy,
        }
    }

    /// Creates an empty instance of `DisjointSets` with a specific strategy to be used by the
    /// `find_set` operation.
    ///
    /// See [FindStrategy](crate::disjoint_sets::FindStrategy).
    ///
    /// # Example
    ///
    /// ```
    /// use union_find_rs::prelude::*;
    ///
    /// let mut sets_1: DisjointSets<usize> = DisjointSets::with_capacity_strategy(512, FindStrategy::PathCompression);
    ///
    /// let mut sets_2: DisjointSets<usize> = DisjointSets::with_capacity_strategy(512, FindStrategy::PathSplitting);
    /// ```
    pub fn with_capacity_strategy(capacity: usize, strategy: FindStrategy) -> Self {
        Self {
            nodes: HashMap::with_capacity(capacity),
            strategy,
        }
    }

    /// Creates an empty instance of `DisjointSets` with the specified capacity.
    ///
    /// The instance returned by this method will be able to hold at least `capacity` elements without reallocating.
    /// If capacity is 0, the hash map will not allocate.
    ///
    /// # Example
    ///
    /// ```
    /// use union_find_rs::prelude::*;
    ///
    /// let mut sets: DisjointSets<usize> = DisjointSets::with_capacity(64);
    /// ```
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            nodes: HashMap::with_capacity(capacity),
            strategy: Default::default(),
        }
    }

    /// Returns the number of elements in this instance of `DisjointSets`.
    ///
    /// # Example
    ///
    /// ```
    /// use union_find_rs::prelude::*;
    ///
    /// let mut sets: DisjointSets<usize> = DisjointSets::new();
    ///
    /// sets.make_set(9).unwrap();
    ///
    /// assert_eq!(sets.len(), 1);
    /// ```
    pub fn len(&self) -> usize {
        self.nodes.len()
    }

    /// Returns `true` if this instance of `DisjointSets` contains `item`, `false` otherwise.
    ///
    /// # Example
    ///
    /// ```
    /// use union_find_rs::prelude::*;
    ///
    /// let mut sets: DisjointSets<usize> = DisjointSets::new();
    ///
    /// sets.make_set(9).unwrap();
    ///
    /// assert_eq!(sets.contains(&9), true);
    /// assert_eq!(sets.contains(&7), false);
    /// ```
    pub fn contains(&self, item: &T) -> bool {
        self.nodes.contains_key(item)
    }

    /// Clears this `DisjointSets`, removing all data. Keeps the allocated memory for reuse.
    ///
    /// # Example
    ///
    /// ```
    /// use union_find_rs::prelude::*;
    ///
    /// let mut sets: DisjointSets<usize> = DisjointSets::new();
    ///
    /// sets.make_set(9).unwrap();
    /// assert_eq!(sets.len(), 1);
    ///
    /// sets.clear();
    /// assert_eq!(sets.len(), 0);
    /// ```
    pub fn clear(&mut self) {
        self.nodes.clear()
    }

    /// Reserves capacity for at least additional more elements to be inserted in this `DisjointSets`.
    /// May reserve more space to avoid frequent reallocations.
    ///
    /// # Panics
    ///
    /// Panics if the new allocation size overflows usize.
    ///
    /// # Example
    ///
    /// ```
    /// use union_find_rs::prelude::*;
    ///
    /// let mut sets: DisjointSets<usize> = DisjointSets::new();
    ///
    /// sets.reserve(16);
    /// ```
    pub fn reserve(&mut self, additional: usize) {
        self.nodes.reserve(additional)
    }

    /// Shrinks the capacity of this `DisjointSets` as much as possible.
    /// It will drop down as much as possible while maintaining the internal
    /// rules and possibly leaving some space in accordance with the resize policy.
    ///
    /// # Example
    ///
    /// ```
    /// use union_find_rs::prelude::*;
    ///
    /// let mut sets: DisjointSets<usize> = DisjointSets::with_capacity(100);
    ///
    /// sets.make_set(4).unwrap();
    /// sets.make_set(9).unwrap();
    /// assert!(100 <= sets.capacity());
    ///
    /// sets.shrink_to_fit();
    /// assert!(2 <= sets.capacity());
    /// ```
    pub fn shrink_to_fit(&mut self) {
        self.nodes.shrink_to_fit()
    }

    /// Returns the number of elements the map can hold without reallocating.
    /// This number is a lower bound; the HashMap<K, V> might be able to hold more,
    /// but is guaranteed to be able to hold at least this many.
    ///
    /// # Example
    ///
    /// ```
    /// use union_find_rs::prelude::*;
    ///
    /// let mut sets: DisjointSets<usize> = DisjointSets::with_capacity(100);
    ///
    /// assert!(100 <= sets.capacity());
    /// ```
    pub fn capacity(&self) -> usize {
        self.nodes.capacity()
    }

    // return the `Node` representation of `item`, if it exists
    fn get_node(&self, item: &T) -> Result<&Node<T>> {
        self.nodes.get(item).ok_or(Error::ItemDoesNotExist)
    }

    // merge the two sets for which `x` and `y` are representatives
    fn link(&self, x: &T, y: &T) -> Result<()> {
        // assert that `x` and `y` are indeed root nodes
        debug_assert!(self.get_node(&x).unwrap().get_parent().is_none());
        debug_assert!(self.get_node(&y).unwrap().get_parent().is_none());
        // `x` should != `y` because of the check in `DisjointSets::union`
        debug_assert!(x != y);
        //
        match self.nodes.contains_key(&x) && self.nodes.contains_key(&y) {
            // both items exist, so link them
            true => {
                // node representations of the items
                let x_node = self.get_node(&x).unwrap();
                let y_node = self.get_node(&y).unwrap();
                // ranks of both of the nodes
                let x_rank = x_node.get_rank();
                let y_rank = y_node.get_rank();

                // union-by-rank
                match x_rank.cmp(&y_rank) {
                    // node with the larger rank becomes the parent
                    Greater => y_node.set_parent(&x_node),
                    Less => x_node.set_parent(&y_node),
                    // if both ranks are equal, then choose the parent arbitrarily
                    // and increment the parent's rank by 1
                    Equal => {
                        x_node.set_parent(&y_node);
                        y_node.set_rank(y_rank + 1);
                    }
                }
                //
                Ok(())
            }
            // one or more of the items does not exist, so error
            false => Err(Error::ItemDoesNotExist),
        }
    }

    // this method follows the semantics of the pseudo code more closely
    //
    // `Node::<T>::get_parent` returns `None` if the node is a root node, whereas in the pseudo
    // code root nodes return themselves when their parents are queried
    //
    // this method thus returns `Ok(t)` when called on `t` and `t` is a root node, and returns an
    // `Err` only if `t` does not exist in this forest
    fn get_parent<'a>(&'a self, item: &'a Node<T>) -> Result<&'a Node<T>> {
        match item.get_parent() {
            //
            Some(parent) => self.get_node(&parent),
            // if `item` doesn't have a parent, return `item`
            None => Ok(item),
        }
    }

    // do `find_set` by doing full path compression
    fn find_set_path_compression(&self, item: &T) -> Result<T> {
        // `item` exists in this `DisjointSets` and is stored in `node`
        let node = self.get_node(&item)?;
        let parent = self.get_parent(node)?;

        // check if `node` is a root node
        match node == parent {
            // `node` is a root node, so just return itself
            true => Ok(node.get_item()),
            // `node` is not a root node, so find the representative and do path
            // compression
            false => {
                // 1. find the representative by recursing
                let representative_item = self.find_set(&parent.get_item())?;
                let representative = self.get_node(&representative_item)?;
                // 2. set the parent of `node` to the representative
                node.set_parent(&representative);
                //
                Ok(representative.get_item())
            }
        }
    }

    // do `find_set` by doing path splitting
    fn find_set_path_splitting(&self, item: &T) -> Result<T> {
        //
        let node = self.get_node(&item)?;

        // `item` exists in this `DisjointSets` and is stored in `node`
        debug_assert!(item == &node.get_item());

        //
        let mut x = node;
        let mut x_parent = self.get_parent(node)?;

        // while `x` is not root
        while x != x_parent {
            //
            let x_parent_parent = self.get_parent(x_parent)?;
            if x_parent != x_parent_parent {
                x_parent.set_parent(x_parent_parent);
            }
            x = x_parent;
            x_parent = x_parent_parent;
        }
        Ok(x.get_item())
    }
}

/// In order to allow easy access to the disjoint sets stored in the forest, this struct implements
/// `IntoIterator`:
/// ```
/// use union_find_rs::prelude::*;
/// use std::collections::HashSet;
///
/// let mut sets: DisjointSets<usize> = DisjointSets::new();
///
/// sets.make_set(1).unwrap();
/// sets.make_set(4).unwrap();
/// sets.make_set(9).unwrap();
///
/// sets.union(&4, &9).unwrap();
///
/// // `into_iter` returns an iterator whose item is of type `HashSet<T>`
/// let as_vec: Vec<HashSet<usize>> = sets.into_iter().collect();
///
/// // there should be 2 disjoint sets, where one of them only contains `4` and the other one
/// // only contains `9`
/// assert_eq!(as_vec.len(), 2);
/// assert!(as_vec
///     .iter()
///     .any(|set| set == &vec![1].into_iter().collect::<HashSet<usize>>()));
/// assert!(as_vec
///     .iter()
///     .any(|set| set == &vec![4, 9].into_iter().collect::<HashSet<usize>>()));
/// ```
impl<T> IntoIterator for DisjointSets<T>
where
    T: Copy + Eq + Hash,
{
    type Item = HashSet<T>;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        // mapping from representatives to the items in their sets
        let rep_to_items: HashMap<T, HashSet<T>> =
            self.nodes
                .iter()
                .fold(HashMap::with_capacity(self.len()), |mut acc, (k, _)| {
                    let parent = self.find_set(k).unwrap();
                    if !acc.contains_key(&parent) {
                        acc.insert(parent, HashSet::new());
                    }

                    acc.get_mut(&parent).unwrap().insert(*k);
                    acc
                });

        // just collect the sets
        rep_to_items
            .into_iter()
            .map(|(_, v)| v)
            .collect::<Vec<_>>()
            .into_iter()
    }
}

//
#[cfg(test)]
mod tests {
    use crate::prelude::*;
    use std::{
        collections::{hash_map::DefaultHasher, HashSet},
        hash::{Hash, Hasher},
    };

    // 2^16 = 65536
    const ITEM_COUNT: usize = 1 << 16;

    // create a
    macro_rules! test_with_different_types {
        //
        ( $mod_name:ident, $test_fn_name:ident, $disjoint_sets:expr ) => {
            //
            #[cfg(test)]
            mod $mod_name {
                use super::*;

                //
                #[test]
                fn test_with_usize() {
                    //
                    let items: Vec<usize> = (0..ITEM_COUNT).collect();
                    let dsets = $disjoint_sets;
                    //
                    $test_fn_name(items, dsets);
                }

                //
                #[test]
                fn test_with_i64() {
                    //
                    const ITEM_COUNT_I64: i64 = ITEM_COUNT as i64;
                    let items: Vec<i64> = (-ITEM_COUNT_I64..ITEM_COUNT_I64).collect();
                    let dsets = $disjoint_sets;
                    //
                    $test_fn_name(items, dsets);
                }

                //
                #[test]
                fn test_with_copyable_structs() {
                    //
                    #[derive(Copy, Clone, PartialEq, Eq, Hash)]
                    struct CustomStruct {
                        id: usize,
                    }
                    //
                    let items: Vec<CustomStruct> =
                        (0..ITEM_COUNT).map(|i| CustomStruct { id: i }).collect();
                    let dsets = $disjoint_sets;
                    //
                    $test_fn_name(items, dsets);
                }

                //
                #[test]
                fn test_noncopyable() {
                    //
                    #[derive(PartialEq, Eq, Hash)]
                    struct CustomStruct {
                        id: usize,
                    }
                    //
                    let items: Vec<CustomStruct> =
                        (0..ITEM_COUNT).map(|i| CustomStruct { id: i }).collect();
                    //
                    let refs: Vec<&CustomStruct> = items.iter().collect();
                    let dsets = $disjoint_sets;
                    //
                    $test_fn_name(refs, dsets);
                }
            }
        };
    }

    //
    fn path_compression_tester<T>(items: Vec<T>, mut dsets: DisjointSets<T>)
    where
        T: Copy + Eq + Hash,
    {
        //
        items.iter().for_each(|item| dsets.make_set(*item).unwrap());

        //
        items.iter().enumerate().skip(1).for_each(|(i, item)| {
            let prev_item: &T = items.get(i - 1).unwrap();
            dsets.union(item, prev_item).unwrap();
        });

        // check that all items have the same representative
        let reps: HashSet<_> = items
            .iter()
            .map(|item| dsets.find_set(item).unwrap())
            .collect();
        //
        assert_eq!(reps.len(), 1);
    }

    //
    fn make_set_only_tester<T>(items: Vec<T>, mut dsets: DisjointSets<T>)
    where
        T: Copy + Eq + Hash,
    {
        //
        items.iter().for_each(|item| dsets.make_set(*item).unwrap());

        // check that all items have the same representative
        let reps: HashSet<_> = items
            .iter()
            .map(|item| dsets.find_set(item).unwrap())
            .collect();
        //
        assert_eq!(reps.len(), items.len());
    }

    //
    fn union_all_tester<T>(items: Vec<T>, mut dsets: DisjointSets<T>)
    where
        T: Copy + Eq + Hash,
    {
        //
        items.iter().for_each(|item| dsets.make_set(*item).unwrap());

        //
        items.iter().enumerate().skip(1).for_each(|(i, item)| {
            dsets.union(item, items.get(i - 1).unwrap()).unwrap();
        });

        // check that there is only 1 set
        let sets: Vec<HashSet<T>> = dsets.into_iter().collect();
        assert_eq!(sets.len(), 1);
    }

    //
    fn hash<T>(t: &T) -> u64
    where
        T: Hash,
    {
        let mut s = DefaultHasher::new();
        t.hash(&mut s);
        s.finish()
    }
    //
    fn union_by_hash_parity_tester<T>(items: Vec<T>, mut dsets: DisjointSets<T>)
    where
        T: Copy + Eq + Hash,
    {
        //
        items.iter().for_each(|item| dsets.make_set(*item).unwrap());

        let mut hash_mod_even = None;
        let mut hash_mod_odd = None;

        //
        items.iter().for_each(|item| {
            if hash(&item) % 2 == 0 {
                match hash_mod_even {
                    Some(x) => dsets.union(item, x).unwrap(),
                    None => {
                        hash_mod_even = Some(item);
                    }
                }
            } else {
                match hash_mod_odd {
                    Some(x) => dsets.union(item, x).unwrap(),
                    None => {
                        hash_mod_odd = Some(item);
                    }
                }
            }
        });

        // check that all items have the same representative
        let sets: Vec<HashSet<T>> = dsets.into_iter().collect();
        //
        assert_eq!(sets.len(), 2);
        assert!(sets.iter().any(|set| set.iter().all(|x| hash(x) % 2 == 0)));
        assert!(sets.iter().any(|set| set.iter().all(|x| hash(x) % 2 == 1)));
    }

    //
    #[cfg(test)]
    mod construct_with_new {
        use super::*;
        //
        test_with_different_types!(make_set_only, make_set_only_tester, DisjointSets::new());
        test_with_different_types!(
            union_by_hash_parity,
            union_by_hash_parity_tester,
            DisjointSets::new()
        );
        test_with_different_types!(
            path_compression,
            path_compression_tester,
            DisjointSets::new()
        );
        test_with_different_types!(union_all, union_all_tester, DisjointSets::new());
    }

    //
    #[cfg(test)]
    mod construct_with_capacity {
        use super::*;
        //
        test_with_different_types!(
            make_set_only,
            make_set_only_tester,
            DisjointSets::with_capacity(ITEM_COUNT)
        );
        test_with_different_types!(
            union_by_hash_parity,
            union_by_hash_parity_tester,
            DisjointSets::with_capacity(ITEM_COUNT)
        );
        test_with_different_types!(
            path_compression,
            path_compression_tester,
            DisjointSets::with_capacity(ITEM_COUNT)
        );
        test_with_different_types!(
            union_all,
            union_all_tester,
            DisjointSets::with_capacity(ITEM_COUNT)
        );
    }

    //
    #[cfg(test)]
    mod construct_with_path_compression {
        use super::*;
        //
        //
        test_with_different_types!(
            make_set_only,
            make_set_only_tester,
            DisjointSets::with_strategy(FindStrategy::PathCompression)
        );
        test_with_different_types!(
            union_by_hash_parity,
            union_by_hash_parity_tester,
            DisjointSets::with_strategy(FindStrategy::PathCompression)
        );
        test_with_different_types!(
            path_compression,
            path_compression_tester,
            DisjointSets::with_strategy(FindStrategy::PathCompression)
        );
        test_with_different_types!(
            union_all,
            union_all_tester,
            DisjointSets::with_strategy(FindStrategy::PathCompression)
        );
    }

    //
    #[cfg(test)]
    mod construct_with_path_splitting {
        use super::*;
        //
        //
        test_with_different_types!(
            make_set_only,
            make_set_only_tester,
            DisjointSets::with_strategy(FindStrategy::PathSplitting)
        );
        test_with_different_types!(
            union_by_hash_parity,
            union_by_hash_parity_tester,
            DisjointSets::with_strategy(FindStrategy::PathSplitting)
        );
        test_with_different_types!(
            union_all,
            union_all_tester,
            DisjointSets::with_strategy(FindStrategy::PathSplitting)
        );
    }

    //
    #[cfg(test)]
    mod construct_with_capacity_path_compression {
        use super::*;
        //
        //
        test_with_different_types!(
            make_set_only,
            make_set_only_tester,
            DisjointSets::with_capacity_strategy(ITEM_COUNT, FindStrategy::PathCompression)
        );
        test_with_different_types!(
            union_by_hash_parity,
            union_by_hash_parity_tester,
            DisjointSets::with_capacity_strategy(ITEM_COUNT, FindStrategy::PathCompression)
        );
        test_with_different_types!(
            path_compression,
            path_compression_tester,
            DisjointSets::with_capacity_strategy(ITEM_COUNT, FindStrategy::PathCompression)
        );
        test_with_different_types!(
            union_all,
            union_all_tester,
            DisjointSets::with_capacity_strategy(ITEM_COUNT, FindStrategy::PathCompression)
        );
    }

    //
    #[cfg(test)]
    mod construct_with_capacity_path_splitting {
        use super::*;
        //
        //
        test_with_different_types!(
            make_set_only,
            make_set_only_tester,
            DisjointSets::with_capacity_strategy(ITEM_COUNT, FindStrategy::PathSplitting)
        );
        test_with_different_types!(
            union_by_hash_parity,
            union_by_hash_parity_tester,
            DisjointSets::with_capacity_strategy(ITEM_COUNT, FindStrategy::PathSplitting)
        );
        test_with_different_types!(
            path_compression,
            path_compression_tester,
            DisjointSets::with_capacity_strategy(ITEM_COUNT, FindStrategy::PathSplitting)
        );
        test_with_different_types!(
            union_all,
            union_all_tester,
            DisjointSets::with_capacity_strategy(ITEM_COUNT, FindStrategy::PathSplitting)
        );
    }
}

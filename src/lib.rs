//! Implementations of [the disjoint-set forest data structure that supports the union-find
//! algorithm](https://en.wikipedia.org/wiki/Disjoint-set_data_structure).
//!
//! # Background / Context
//!
//! 1. [Wikipedia article: disjoint-set data structure](https://en.wikipedia.org/wiki/Disjoint-set_data_structure)
//!
//! # Getting Started
//!
//! 1. `use union_find::prelude::*;` to import everything necessary
//! 1. see the trait [`UnionFind`](crate::traits::UnionFind) for the core operations of union-find
//! 1. see the struct [`DisjointSets`](crate::disjoint_sets::DisjointSets) for an implementation of
//! [`UnionFind`](crate::traits::UnionFind)
//!
//! # Example
//!
//! ```
//! use std::collections::HashSet;
//! use union_find_rs::prelude::*;
//!
//! let mut sets: DisjointSets<usize> = DisjointSets::new();
//!
//! sets.make_set(1).unwrap();
//! sets.make_set(4).unwrap();
//! sets.make_set(9).unwrap();
//!
//! sets.union(&1, &4).unwrap();
//!
//! // the disjoint sets as a vector of vectors
//! let as_vec: Vec<HashSet<usize>> = sets.into_iter().collect();
//!
//! // there should be 2 disjoint sets, where one of them only contains `9` and the other one
//! // only contains `1` and `4`
//! assert_eq!(as_vec.len(), 2);
//! assert!(
//!     as_vec
//!         .iter()
//!         .any(|set| set == &vec![9].into_iter().collect::<HashSet<usize>>())
//! );
//! assert!(
//!     as_vec
//!         .iter()
//!         .any(|set| set == &vec![1, 4].into_iter().collect::<HashSet<usize>>())
//! );
//! ```

pub mod disjoint_sets;
mod node;
pub mod prelude;
pub mod traits;

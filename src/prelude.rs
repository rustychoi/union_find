//! Imports everything necessary to get started. `use union_find::prelude::*;` provides easy access to
//! various traits and structs you will need.

pub use crate::{
    disjoint_sets::{DisjointSets, FindStrategy},
    traits::{Error, Result, UnionFind},
};
